<?php





/////////////////////////////////
// no longer in use...........
/////////////////////////////
/////////////////////////////

require_once "User.php";
require_once "Patient.php";
require_once "Connection.php";
require_once "Visit.php";
require_once "Question.php";
require_once "Answer.php";
require_once "Agency.php";
require_once "User.php";
require_once "Patient.php";
require_once "GUID.php";


class Telephony
{

    protected $wait_time=10000;
    protected $pause=3000;

    protected $agi;
    protected $config;
    protected $mongo;
    protected $sql;

    protected $dead_channel_code=511; // error cade for dead channel returned by asterisk AMI
    protected $time_margin=15; // can check in or check out 15 mins aearly or 15 mins later

    //Sounds
    protected $greet_file;
    protected $menu_file;
    protected $main_menu_file; // "pronounces main menu"
    protected $clock_in_menu_file;
    protected $complete_menu_file;
    protected $calling_from_file;
    protected $calling_from_confirm_file;
    protected $no_valid_apt_file;
    protected $task_number_file;
    protected $record_prefix_file;
    protected $record_general_file;
    protected $record_reason_file;
    protected $record_milage_file;
    protected $clock_out_file;
    protected $yesno_file;
    protected $yesno_only_file;
    protected $pin_file;
    protected $invalid_pin_file;
    protected $invalid_pin_entry_file;
    protected $you_entered_file;
    protected $phone_not_registered_file;
    protected $contact_case_manager_file;
    protected $marked_completed_file;
    protected $task_not_complete_file;
    protected $task_list_complete_file;
    protected $sound_path;
    protected $clocked_in_at_file;
    protected $clocked_out_at_file;
    protected $cst_file;

    public function __construct($agi)
    {
        $this->agi=$agi;
    }

    public function init($config, $db_config)
    {
        $this->config=$config;
        $this->mongo=$db_config['mongo'];
        $this->sql=$db_config['default'];

        //Sounds
        if(isset($config['greet_file'])){$this->greet_file=$config['greet_file']; }
        if(isset($config['menu_file'])){$this->menu_file=$config['menu_file']; }
        if(isset($config['main_menu_file'])){$this->main_menu_file=$config['main_menu_file']; }
        if(isset($config['clock_in_menu_file'])){$this->clock_in_menu_file=$config['clock_in_menu_file']; }
        if(isset($config['complete_menu_file'])){$this->complete_menu_file=$config['complete_menu_file']; }
        if(isset($config['calling_from_file'])){$this->calling_from_file=$config['calling_from_file'];}
        if(isset($config['calling_from_confirm_file'])){$this->calling_from_confirm_file=$config['calling_from_confirm_file'];}
        if(isset($config['no_valid_apt_file'])){$this->no_valid_apt_file=$config['no_valid_apt_file'];}
        if(isset($config['task_number_file'])){$this->task_number_file=$config['task_number_file']; }
        if(isset($config['sound_file_asterisk'])){$this->sound_path=$config['sound_file_asterisk']; }
        if(isset($config['record_prefix_file'])){	$this->record_prefix_file=$config['record_prefix_file']; }
        if(isset($config['record_general_file'])){$this->record_general_file=$config['record_general_file']; }
        if(isset($config['record_reason_file'])){$this->record_reason_file=$config['record_reason_file']; }
        if(isset($config['clock_out_file'])){	$this->clock_out_file=$config['clock_out_file']; }
        if(isset($config['record_milage_file'])){$this->record_milage_file=$config['record_milage_file']; }
        if(isset($config['wait_time'])){$this->wait_time=$config['wait_time'];}
        if(isset($config['yesno_file'])){$this->yesno_file=$config['yesno_file']; }
        if(isset($config['yesno_only_file'])){$this->yesno_only_file=$config['yesno_only_file']; }
        if(isset($config['pin_file'])){$this->pin_file=$config['pin_file']; }
        if(isset($config['invalid_pin_file'])){$this->invalid_pin_file=$config['invalid_pin_file']; }
        if(isset($config['invalid_pin_entry_file'])){$this->invalid_pin_entry_file=$config['invalid_pin_entry_file']; }
        if(isset($config['you_entered_file'])){$this->you_entered_file=$config['you_entered_file']; }
        if(isset($config['phone_not_registered_file'])){$this->phone_not_registered_file=$config['phone_not_registered_file']; }
        if(isset($config['contact_case_manager_file'])){$this->contact_case_manager_file=$config['contact_case_manager_file']; }
        if(isset($config['marked_completed_file'])){$this->marked_completed_file=$config['marked_completed_file']; }
        if(isset($config['task_not_complete_file'])){$this->task_not_complete_file=$config['task_not_complete_file']; }
        if(isset($config['task_list_complete_file'])){$this->task_list_complete_file=$config['task_list_complete_file']; }
        if(isset($config['clocked_in_at_file'])){$this->clocked_in_at_file=$config['clocked_in_at_file']; }
        if(isset($config['clocked_out_at_file'])){$this->clocked_out_at_file=$config['clocked_out_at_file']; }
        if(isset($config['cst_file'])){$this->cst_file=$config['cst_file']; }

    }

    public function mainLoop()
    {

        $con=new Connection($this->sql);
        $db=$con->connect();
        $conMon=new Connection($this->mongo);
        $dbMon=$conMon->mongoConnect();

        $collection=$dbMon->questionsets;

        $visit=new Visit($db,$this->time_margin);
        $question=new Question($db, $dbMon);
        $answer=new Answer($db, $dbMon);
        $agency=new Agency($db, $collection);
        $user=new User($db, $collection);
        $patient=new Patient($db, $collection);
        $guid=new GUID();

        $validatedCount=0;
        $clockedIn=false;

        $this->greet();

        //getting users or patients assiciated with calling phone
        $caller_data=$this->isReady();

        if($caller_data['type']=='none') {
            $this->phoneNotRegistered();
            return;
        }
        elseif($caller_data['type']=='patient') {
            $pins=$this->getUserPinByPatient($caller_data['ids']);
            if($this->comparePin($pins)) {
                $visits=$visit->getVisitByPatient($caller_data['ids']);
                goto validated;
            }
            else {
                return;
            }
        }
        elseif($caller_data['type']=='user') {
            $pins=$this->getUserPinByUser($caller_data['ids']);
            if($this->comparePin($pins)) {
                $visits=$visit->getVisitByUser($caller_data['ids']);
                goto validated;
            }
            else {
                return;
            }
        }


        validated:
            //checking state of the visit
            if(count($visits)==1) {
                $visitId=$visits[0]->id;
                $clockedIn=$visits[0]->isClockedIn;
                $clockedOut=$visits[0]->isClockedOut;
                $user_id=$visits[0]->user_id;
                $patient_id=$visits[0]->patient_id;
                $ques_ref_id=$visits[0]->question_ref_id; // question id of the agency , known as reference_id
                $agency_id=$visits[0]->agency_id;
                $agency_ref_id=$agency->getRefIdById($agency_id);
                $user_ref_id=$user->getRefId($user_id);
                $patient_ref_id=$patient->getRefId($patient_id);
                $zone=$visits[0]->time_zone;
                $visit_question=$question->getQuestionByAgencyAndId($agency_ref_id, $ques_ref_id);
                $visit_answer=$answer->getAnswer($agency_id, $ques_ref_id, $visit_question, $visits[0]);
                $visit_sentQA=$visits[0]->sentQA;

                if($question->isComplete( $visit_question) && $clockedOut==1) {
                    goto completeMenu;
                }

                if($clockedIn==0)
                    goto entryMenu;
                elseif($clockedIn==1 && $clockedOut==0)
                    goto clockedInMenu;
                elseif($clockedIn==1 && $clockedOut==1)
                    goto completeMenu;
                else
                    return;
            }
            else
            {
                if(count($visits) > 1) {
                    goto multipleVisits;
                }
                else {
                    $this->noValidAppointment();
                    return;
                }
            }


        multipleVisits:
            $selectedVisit=$this->getVisit($visits);
            print_r($selectedVisit);
            if($selectedVisit==null){
                return;
            }
            unset($visits);
            $visits=array();
            $visits[0]=$selectedVisit;
            goto validated;


        entryMenu:
            $key=-1;
            while($key==-1){
                if($clockedIn==1){
                    goto clockedInMenu;
                }
                $key=$this->entryMenu();

                if($key==0){
                    return;
                }
            }

            if($key==1) {
                if($visit->clockIn($visitId, $zone)) {
                    $clockedIn=1;
                    $this->agi->stream_file($this->clocked_in_at_file);
                    $this->agi->say_time();
                    $this->agi->stream_file($this->cst_file);
                    $this->agi->stream_file($this->main_menu_file);
                    goto clockedInMenu;
                }
                else {
                    goto entryMenu;
                }
            }

            if($key==2) {
                $this->playList($visit_question);
                $this->agi->stream_file($this->main_menu_file);
                goto entryMenu;
            }

            if($key==3) {
                invalidMilage:
                    $keys=$this->recordMilage();
                    if(is_numeric($keys) && $keys!=-1) {
                        $visit->updateMilage($visitId, $keys);
                        $this->agi->stream_file($this->you_entered_file);
                        $this->sayNumber($keys);
                        $this->agi->stream_file('miles');
                        $this->agi->stream_file('vm-and');
                        $this->agi->stream_file('vm-saved');
                        $this->agi->stream_file($this->main_menu_file);
                        goto entryMenu;
                    }
                    else {
                        $this->agi->stream_file('privacy-incorrect');
                        //$this->agi->stream_file($this->main_menu_file);
                        goto invalidMilage;
                    }
            }


        clockedInMenu:
            $key=-1;
            while($key==-1) {
                $key=$this->clockedInMenu();
            }

            if($key==1) {
                $this->playList($visit_question);
                $this->agi->stream_file($this->main_menu_file);
                goto clockedInMenu;

                if($key==0) {
                    return;
                }
            }

            if($key==2) {
                if($question->isComplete( $visit_question)) {
                    // play task list complete sound file
                    $this->agi->stream_file($this->task_list_complete_file);
                    $this->agi->stream_file($this->main_menu_file);
                    goto clockedInMenu;
                }
                $visit_question=$this->playQuestions($visit_question, $visit_answer);
                $this->agi->stream_file($this->main_menu_file);
                goto clockedInMenu;
            }

            if($key==3) {
                invalidMilage2:
                    $keys=$this->recordMilage();
                    if(is_numeric($keys) && $keys!=-1) {
                        $visit->updateMilage($visitId, $keys);
                        $this->agi->stream_file($this->you_entered_file);
                        $this->sayNumber($keys);
                        $this->agi->stream_file('miles');
                        $this->agi->stream_file('vm-and');
                        $this->agi->stream_file('vm-saved');
                        $this->agi->stream_file($this->main_menu_file);
                        goto clockedInMenu;
                    }
                    else {
                        $this->agi->stream_file('privacy-incorrect');
                        goto invalidMilage2;
                    }
            }

            if($key==4) {
                $visit_question=$question->getQuestionByAgencyAndId($agency_ref_id, $ques_ref_id);
                if($question->isComplete($visit_question)) {
                    $visit->clockOut($visitId, $zone);
                    $this->agi->stream_file($this->clocked_out_at_file);
                    $this->agi->say_time();
                    $this->agi->stream_file($this->cst_file);
                    $this->agi->stream_file($this->main_menu_file);
                    goto completeMenu;
                }
                else {
                    confirm:
                        $ret=$this->getComfirmationNotComplete();
                        if($ret==49) {
                            $visit->clockOut($visitId, $zone);
                            $this->agi->stream_file($this->clocked_out_at_file);
                            $this->agi->say_time();
                            $this->agi->stream_file($this->cst_file);
                            $this->agi->stream_file($this->main_menu_file);
                            goto completeMenu;
                        }
                        elseif($ret==50) {
                            $this->agi->stream_file($this->main_menu_file);
                            goto clockedInMenu;
                        }
                        else {
                            goto confirm;
                        }
                }
            }
            if($key==5) {
                $this->storeGeneralComments($visit_question);
                $this->agi->stream_file($this->main_menu_file);
                goto clockedInMenu;
            }
            goto end;


        completeMenu:
            if($visit_sentQA==0) {
                $key=-1;
                while($key==-1) {
                $key=$this->completeMenu();

                if($key==0)
                return;
                }
                if($key==1) {
                $this->storeGeneralComments($visit_question);
                $this->agi->stream_file($this->main_menu_file);
                goto completeMenu;
                }

                if($key==2) {
                    invalidMilage3:
                        $keys=$this->recordMilage();
                        if(is_numeric($keys) && $keys!=-1) {
                            $visit->updateMilage($visitId, $keys);
                            $this->agi->stream_file($this->you_entered_file);
                            $this->sayNumber($keys);
                            $this->agi->stream_file('miles');
                            $this->agi->stream_file('vm-and');
                            $this->agi->stream_file('vm-saved');
                            $this->agi->stream_file($this->main_menu_file);
                            goto completeMenu;
                        }
                        else {
                            $this->agi->stream_file('privacy-incorrect');
                            goto invalidMilage3;
                        }

                }
                if($key==3) {
                    $visit->sendQA($visitId);
                    $this->agi->stream_file($this->config['sound_file_core']."sent_qa_success");
                    $this->agi->stream_file('vm-goodbye');
                }

            }
            else {
                $this->agi->stream_file($this->config['sound_file_core']."already_qa");
                $this->agi->stream_file('vm-goodbye');
            }

        end:
    }

    public function entryMenu()
    {
        $channel_status=$this->agi->channel_status();
        if($channel_status['code']==$this->dead_channel_code) { // channel hung up
            return 0;
        }
        // $this->agi->stream_file($this->main_menu_file);
        $char=$this->agi->stream_file($this->menu_file, "# 1 2 3");
        $pressed=chr($char['result']);
        if(empty($char['result'])) {
            $char=$this->agi->wait_for_digit($timeout=$this->wait_time);
            if(!empty($char['result'])) {
                $pressed=chr($char['result']);
            }
            else {
                $pressed="0";
            }
        }
        return $pressed;
    }

    public function clockedInMenu()
    {
        $channel_status=$this->agi->channel_status();
        if( $channel_status['code']==$this->dead_channel_code) { // channel hung up
            return 0;
        }
        $char=$this->agi->stream_file($this->clock_in_menu_file, "# 1 2 3 4 5");
        $pressed=chr($char['result']);
        if(empty($char['result'])) {
            $char=$this->agi->wait_for_digit($timeout=$this->wait_time);
            if(!empty($char['result'])) {
                $pressed=chr($char['result']);
            }
            else {
                $pressed="0";
            }
        }
        return $pressed;
    }

    public function completeMenu()
    {
        $channel_status=$this->agi->channel_status();
        if( $channel_status['code']==$this->dead_channel_code) { // channel hung up
            return 0;
        }
        $this->agi->stream_file($this->marked_completed_file);
        $char=$this->agi->stream_file($this->complete_menu_file, "# 1 2 3");
        $pressed=chr($char['result']);
        if(empty($char['result'])) {
            $char=$this->agi->wait_for_digit($timeout=$this->wait_time);
            if(!empty($char['result'])) {
                $pressed=chr($char['result']);
            }
            else {
                $pressed="0";
            }
        }
        return $pressed;
    }

    public function greet()
    {
        if(isset($this->greet_file)) {
            $this->agi->stream_file($this->greet_file);
        }
    }

    public function getPin()
    {
        $pin=null;
        if(isset($this->pin_file)) {
            $count=0;
            while($count < 1) {
                $ret=$this->agi->get_data($this->pin_file, $timeout=$this->wait_time, $max_digits="4", $escape_digits="#");
                $count++;

                if(!empty($ret['result'])) {
                    $pin=$ret['result'];
                    //$this->agi->stream_file($this->you_entered_file);
                    //$this->sayDigits($pin);
                    //$yesno=$this->agi->get_data($this->yesno_only_file, $max_digits="1", $timeout=$this->wait_time);
                    //if($yesno['result']==1){
                    //    break;
                    //}
                }
            }
            return $pin;
        }
    }

    public function comparePin($pins)
    {
        $validatedCount=0;
        if(count($pins) > 0) {
            tryValidate:
                $personel_pin=$this->getPin();
                $validatedCount++;
                if(in_array($personel_pin, $pins)) {
                    return true;
                }
                else {
                    if($validatedCount > 2) {
                        $this->agi->stream_file($this->invalid_pin_file);
                        return false;
                    }
                    else {
                        $this->agi->stream_file($this->invalid_pin_entry_file);
                        goto tryValidate;
                    }
                }
        }
    }

    public function sayDigits($digits)
    {
        $this->agi->say_digits($digits);
    }

    public function sayNumber($number)
    {
        $this->agi->say_number($number);
    }

    public function noValidAppointment()
    {
        if(isset($this->no_valid_apt_file))
        {
            $this->agi->stream_file($this->no_valid_apt_file);
        }
    }

    public function getCallerId()
    {
        return $this->agi->input['agi_callerid'];
    }

    public function reviewTask($task_number, $task)
    {
        if(isset($task_number) && isset($task)) {
            $this->agi->stream_file($this->task_number_file, "1 2");
            $this->sayNumber($task_number);
            $key=$this->agi->stream_file($task, "1 2");
            return chr($key['result']);
        }
    }

    public function recordReason($agency,$question, $question_no, $phone)
    {
        $this->agi->stream_file($this->record_reason_file);
        $this->agi->stream_file($this->record_prefix_file);
        $base=$this->sound_path;
        date_default_timezone_set('America/Chicago');
        $time= date('y-m-d-h-i');
        $file_name="{$base}task_reason_{$agency}_{$question}_{$question_no}_{$phone}_{$time}";
        $this->agi->record_file($file_name, "wav", $escape_digits="#", $timeout="-1", $beep="true", $offset=NULL, $silence="10");
        $nameOnly="task_reason_{$agency}_{$question}_{$question_no}_{$phone}_{$time}";
        return $nameOnly;
    }

    public function recordGeneralComment($agency,$question, $phone)
    {
        $this->agi->stream_file($this->record_general_file);
        $this->agi->stream_file($this->record_prefix_file);
        $base=$this->sound_path;
        date_default_timezone_set('America/Chicago');
        $time= date('y-m-d-h-i');
        $file_name="{$base}general_comments_{$agency}_{$question}_{$phone}_{$time}";
        $this->agi->record_file($file_name, "wav", $escape_digits="#", $timeout="-1", $beep="true", $offset=NULL, $silence="10");
        return $file_name;
    }

    //check user and patient and question in the system
    //then return all these information
    public function isReady()
    {
        $ret_array=array('type'=>'none','ids'=>array());

        $tp=$this->agi->input['agi_callerid'];

        if(isset($tp)) {
            $con=new Connection($this->sql);
            $db=$con->connect();

            $conMon=new Connection($this->mongo);
            $dbMon=$conMon->mongoConnect();
            $collection=$dbMon->questionsets;

            $user=new User($db, $collection);
            $patient=new Patient($db, $collection);

            $user_ids=$user->getIdFromPhone($tp);
            $patient_ids=$patient->getIdFromPhone($tp);

            if(count($user_ids) > 0) {
                $ret_array['type']='user';
                $ret_array['ids']=$user_ids;
            }
            else if(count($patient_ids) > 0) {
                $ret_array['type']='patient';
                $ret_array['ids']=$patient_ids;
            }
        }
        return $ret_array;
    }

    public function getUserPinByUser($user_ids)
    {
        $con=new Connection($this->sql);
        $db=$con->connect();

        $conMon=new Connection($this->mongo);
        $dbMon=$conMon->mongoConnect();
        $collection=$dbMon->questionsets;

        $user=new User($db, $collection);
        $pin=$user->getPin($user_ids);

        return $pin;
    }

    public function getUserPinByPatient($pat_ids)
    {
        $con=new Connection($this->sql);
        $db=$con->connect();

        $conMon=new Connection($this->mongo);
        $dbMon=$conMon->mongoConnect();
        $collection=$dbMon->questionsets;

        $patient=new Patient($db, $collection);
        $pin=$patient->getPin($pat_ids);

        return $pin;
    }

    public function clockOut()
    {
        $char=$this->agi->stream_file($this->check_out_file, "# 1 2 3 4");
        $pressed=chr($char['result']);
        if(empty($char['result'])) {
            $char=$this->agi->wait_for_digit($timeout=$this->wait_time);
            if(!empty($char['result'])) {
                $pressed=chr($char['result']);
            }
            else {
                $pressed="-1";
            }
        }
        return $pressed;
    }

    public function recordMilage()
    {
        $char=$this->agi->get_data($this->record_milage_file, $timeout=$this->wait_time, $max_digits="3");
        if(!empty($char['result'])) {
            $pressed=$char['result'];
        }
        else {
            $pressed="-1";
        }
        return $pressed;
    }

    public function getFilePath($text)
    {
        $base=$this->sound_path;
        $fileName=md5($text);
        $filePath="{$base}{$fileName}";

        return $filePath;
    }

    public function playList($questionset)
    {
        $questions=$questionset['questions'];

        foreach ($questions as $question) {
            $this->agi->stream_file($this->task_number_file, "#");
            $this->sayNumber($question['no']);

            $fileName=md5($question['question']);

            $fullPath=$this->sound_path.$fileName;
            $this->agi->stream_file($fullPath, "#");

        }
    }

    public function playQuestions($questionset, $answerset)
    {
        $ques_ref=$questionset['reference_id'];
        $agency=$questionset['agency_id'];
        $questions=$questionset['questions'];
        $phone=$this->getCallerId();
        $pressed=-1;

        $con=new Connection($this->sql);
        $db=$con->connect();
        $conMon=new Connection($this->mongo);
        $dbMon=$conMon->mongoConnect();

        $questionModel=new Question($db, $dbMon);
        $answerModel=new Answer($db, $dbMon);
        $index=0;

        foreach ($questions as $question) {
            //$this->agi->stream_file($this->task_number_file, "#");
            if($question['answered']=='no') {
                $ret_no=$this->sayNumber($question['no']);

                if(!empty($ret_no['result'])) {
                    $pressed=$ret_no['result'];
                }
                else {
                    $pressed=-1;
                }
                if($pressed==-1) {
                    $fileName=md5($question['question']);
                    $fullPath=$this->sound_path.$fileName;
                    $ret_ques=$this->agi->stream_file($fullPath, "# 1 2");

                    if(!empty($ret_ques['result'])) {
                        $pressed=$ret_ques['result'];
                    }
                    else {
                        $pressed=-1;
                    }
                }
                if($pressed==-1) {
                    $ret_yesno=$this->agi->stream_file($this->yesno_file, "# 1 2");
                    if(!empty($ret_yesno['result'])) {
                        $pressed=$ret_yesno['result'];
                    }
                    else {
                        $pressed=-1;
                    }
                }
                if($pressed==-1) {
                    $ret_wait=$this->agi->wait_for_digit($timeout=-1, "# 1 2");
                    if(!empty($ret_wait['result'])) {
                        $pressed=$ret_wait['result'];
                    }
                    else {
                        $pressed=-1;
                    }
                }
                if($pressed==-1 || $pressed==35) { // # or nothing pressed
                    $index++;
                    continue;
                }
                else {
                    if($pressed==49) { //pressed 1
                        $answerModel->saveAnswer($answerset, $question['no'], "1", "");
                        $questions[$index]['answered']="yes";
                        $questionset['questions']=$questions;
                        $questionModel->updateQuestion($questionset);
                    }
                    if($pressed==50) { //pressed 2
                        $fileName=$this->recordReason($agency, $ques_ref, $question['no'],$phone);
                        $answerModel->saveAnswer($answerset, $question['no'], "2", $fileName);
                        $questions[$index]['answered']="yes";
                        $questionset['questions']=$questions;
                        $questionModel->updateQuestion($questionset);
                    }
                }
            }
            $index++;
        }
        return $questionset;
    }

    public function storeGeneralComments($questionset)
    {
        $ques_ref=$questionset['reference_id'];
        $agency=$questionset['agency_id'];
        $phone=$this->getCallerId();

        $this->recordGeneralComment($agency, $ques_ref,$phone);
    }

    public function phoneNotRegistered()
    {
        $this->agi->stream_file($this->phone_not_registered_file);
        $this->agi->stream_file($this->contact_case_manager_file);
    }

    public function getComfirmationNotComplete()
    {
        $pressed=-1;
        $ret_no=$this->agi->stream_file($this->task_not_complete_file, "# 1 2");

        if(!empty($ret_no['result'])) {
            $pressed=$ret_no['result'];
        }
        else {
            $pressed=-1;
        }
        if($pressed==-1) {
            $ret_yesno=$this->agi->stream_file($this->yesno_only_file, "# 1 2");
            if(!empty($ret_yesno['result'])) {
                $pressed=$ret_yesno['result'];
            }
            else {
                $pressed=-1;
            }
        }
        if($pressed==-1)
        {
            $ret_wait=$this->agi->wait_for_digit($timeout=$this->wait_time, "# 1 2");
            if(!empty($ret_wait['result'])) {
                $pressed=$ret_wait['result'];
            }
            else {
                $pressed=-1;
            }
        }
        return $pressed;
    }

    public function getVisit($visits)
    {
        $index=0;
        $max=count($visits);
        $min=0;
        $count=0;
        //for client file, will not work with asterisk as this is local to this
        $for_client_path=$this->config['temp_sound_file_location'].'for_client.sln';
        //for care giver file
        $care_giver_path=$this->config['temp_sound_file_location'].'care_giver.sln';

        $press=$this->config['temp_sound_file_location'].'press.sln';

        $this->agi->stream_file($this->config['sound_file_core']."multiple_visit");
        foreach($visits as $visit) {
            echo "Count = ".$count;
            $count++;
            $pressed=-1;
            $num=$this->config['temp_sound_file_location']."$count".".sln";

            $user_first=$visit->user_first;
            $user_first_hash=md5($user_first);
            $user_first_path=$this->config['temp_sound_file_location']."names/".$user_first_hash.".sln";

            $user_last=$visit->user_last;
            $user_last_hash=md5($user_last);
            $user_last_path=$this->config['temp_sound_file_location']."names/".$user_last_hash.".sln";


            $patient_first=$visit->patient_first;
            $patient_first_hash=md5($patient_first);
            $patient_first_path=$this->config['temp_sound_file_location']."names/".$patient_first_hash.".sln";

            $patient_last=$visit->patient_last;
            $patient_last_hash=md5($patient_last);
            $patient_last_path=$this->config['temp_sound_file_location']."names/".$patient_last_hash.".sln";

            $rand=rand(0,100000);
            $temp=$this->config['temp_sound_file_location']."temp$rand.sln";
            //generating temp file in the location
            $cmd="cat $press $num $for_client_path $patient_first_path $patient_last_path $care_giver_path $user_first_path $user_last_path > $temp";
            $out=shell_exec($cmd);
            echo $out;
            $char=$this->agi->stream_file($this->config['sound_file_core']."temp$rand", "# 1 2 3 4 5 6 7 8 9");
            if(!empty($char['result'])) {
                $pressed=chr($char['result']);
            }
            else {
                $pressed=-1;
            }
            if($pressed != -1) {
                shell_exec("rm $temp");
                break;
            }

            shell_exec("rm $temp");
        }

        if($pressed==-1) {
            $ret_wait=$this->agi->wait_for_digit($timeout=-1, "# 1 2 3 4 5 6 7 8 9");
            if(!empty($ret_wait['result'])) {
                $pressed=chr($ret_wait['result']);
            }
            else {
                $pressed=-1;
            }
        }

        if($pressed!= -1) {
            $index=$pressed - 1;
        }
        else {
            return null;
        }

        if($index >= $min && $index < $max) {
            return $visits[$index];
        }
        else {
            return null;
        }
    }
}
