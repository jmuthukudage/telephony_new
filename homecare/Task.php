<?php

require_once "Curl.php";

class Task
{
    public $id;
    public $answer;
    protected $visitid;

    protected $api;
    protected $curl;
    protected $log;

    public function __construct($api)
    {
        $this->id = null;
        $this->visitid = null;
        $this->answer = null;

        $this->curl = new Curl();
        $this->api = $api;
    }

    public function setStatus($visitId, $taskId, $status)
    {
    	$url = $this->api . 'task/' . $taskId;
        $resp = $this->curl->put_json($url, array( 'Status' => $status,
        	                                       'TaskId'=>$taskId,
        	                                       'VisitId'=>$visitId));
    }


    public function saveAnswer($visitId, $taskId, $answer)
    {
       $url = $this->api . 'visit/' . $visitId.'/task/'.$taskId.'/answer';
       $resp = $this->curl->post_json($url, array( 'Answer' => $answer,
        	                                       'TaskId'=>$taskId,
        	                                       'VisitId'=>$visitId));
   }

}
