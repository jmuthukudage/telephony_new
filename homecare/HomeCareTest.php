<?php

//This test script is behaving strangely. Need to fix and write more test cases
date_default_timezone_set('America/Chicago');
ini_set('error_reporting', E_ALL);
ini_set('display_errors', true);
require '../vendor/autoload.php';
require_once 'HomeCare.php';
use PAGI\Client\Impl\ClientImpl as PagiClient;
use PAGI\Node\Node;

try{
$config = include 'config.php';
// Go, go, gooo!
$pagiClientOptions = array();
$pagiClient = new \PAGI\Client\Impl\MockedClientImpl($pagiClientOptions);
$pagiAppOptions = array(
    'pagiClient' => $pagiClient,
);
$pagiApp = new HomeCare($pagiAppOptions);
pcntl_signal(SIGHUP, array($pagiApp, 'signalHandler'));
$pagiClient
    ->onAnswer(true)
    ->onGetFullVariable(true, '1234')
    ->onCreateNode('enterPin')
    ->assertSaySound(getSound('welcome'), 1)
    ->runWithInput('12346')
    ->assertSaySound('invalid_pin', 1);
$pagiApp->init();
$pagiApp->run();
}catch(Exception $e){
  echo $e->getMessage();
}

function getSound($name) {
		return $config['sounds_path'] . $name;
	}