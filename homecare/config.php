<?php

$config = array();

$config['database'] = array();

//mysql connection
$config['database']['driver' ] = 'mysql';
$config['database']['host'] = "localhost";
$config['database']['port'] = 3306;
$config['database']['db'] = "telephony_homecare";
$config['database']['user'] = "root";
$config['database']['passwd'] = "Axxess12";

//Absolute path to sound files on the asterisk server
$config['sounds_path'] = "/var/www/html/telephony_sound_files/homecare/sounds/";

//Absolute path for recordings sound files on the asterisk server
$config['recordings_path'] = "/var/www/html/telephony_sound_files/homecare/recordings/";

//Absolute path for task sound files on asterisk server
$config['tasks_path'] = "/var/www/html/telephony_sound_files/homecare/tasks/";

//Absolute path for client name sound files on asterisk server
$config['clients_path'] = "/var/www/html/telephony_sound_files/homecare/clients/";

//How many miliseconds to wait between keys
$config['wait_time'] = 10000;

//max recording time, task not complete reason, general comments
$config['maxtime'] = 1; // one minute is equal to 1

$config['menu_repeats'] = 3;

$config['api'] = "http://172.16.19.209:81/api/";

return $config;
