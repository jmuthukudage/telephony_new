<?php
define('ROOT_PATH', getenv('root')); // Only used inside log4php.properties
use PAGI\Application\PAGIApplication;
use PAGI\Client\Impl\MockedClientImpl;
use PAGI\Node\Node;

declare (ticks = 1);

require_once "User.php";
require_once "Visit.php";
require_once "Task.php";
require_once "Activity.php";

class Status extends SplEnum {
	const __default = self::None;

	const None = 0;
	const Missed = 1;
	const Progress = 2;
	const Finished = 3;
	const Qa = 4;
}

class MissedVisitReason extends SplEnum {
	const __default = self::Other;

    const CancellationofCare = 1;
    const DoctorClinicAppointment = 2;
    const FamilyCaregiverAbletoAssistPatient = 3;
    const NoAnswertoLockedDoor = 4;
    const NoAnswertoPhoneCall = 5;
    const PatientFamilyUncooperative = 6;
    const PatientHospitalized = 7;
    const PatientUnabletoAnswerDoor = 8;
    const TherapyonHold = 9;
	const Other = 99;
}

class TaskStatus extends SplEnum {
    const __default = self::NotComplete;

    const NotComplete = 0;
    const Complete = 1;
   }

class AnswerStatus extends splEnum {
	const __default = self::notAnswered;

	const NotAnswered = 0;
	const Answered = 1;
	const Ignored = 2;
}

class Source extends SplEnum {
    const __default = self::Phone;

    const Phone = 0;
    const User = 1;
   }


class HomeCare extends PAGIApplication {

	protected $agi;
	protected $nodeController = null;
	protected $config;

	protected $user;
	protected $visit = null;
	protected $task = null;
	protected $current_visits=null;
	protected $activities=array();
	protected $clients=array();

	public function init() {

		$this->agi = $this->getAgi();
		$this->logger->debug('HomeCare Init');
		$this->agi->answer();

		$this->config = include 'config.php';

		$this->user = new User($this->logger, $this->config['api']);
		$this->task =  new Task($this->config['api']);
		$this->nodeController = $this->agi->createNodeController('homecare');
       
	}

	public function shutdown() {
		try {
			//make sure we have pushed visit data up
			if ($this->visit) {
				$this->visit->push();
				$this->visit->setInactive();
               	$activity = new Activity($this->logger, $this->config['api'], $this->activities);
				$activity->push($this->visit->Id);
			}
			$this->logger->debug('Shutdown');
			$client = $this->getAgi();
			$client->hangup();
		} catch (\Exception $e) {
		}
	}

	public function run() {
       	$users = $this->user->findUsers($this->agi->getCallerId()->getNumber());
      	//init all menus
		$this->enterPin($users);
       	$this->noVisits();
        $this->multipleVisits();
		$this->sayStarInfo();
		$this->sayVisit();
		$this->entryMainMenu();
		$this->missedMainMenu();
		$this->clockedIn();
		$this->visitMainMenu();
		$this->clockedOut();
		$this->completedMainMenu();
		$this->travelMainMenu();
		$this->reviewTaskList();
		$this->updateTaskList();
		$this->enterMileage();
		$this->enterTravelTime();
		$this->enterReasonCode();
		$this->generalComments();
		
		//Call Starts
		if (empty($users)) {
			//not a valid phone
             echo "not a valid phone..".PHP_EOL;
			$this->agi->streamFile($this->getSound('invalid_phone'));
			$this->agi->streamFile($this->getSound('contact_office'));
			$this->shutdown();
		}
		$this->nodeController->jumpTo('enterPin');
	}

	public function errorHandler($type, $message, $file, $line) {
		$this->logger->debug(
			'ErrorHandler: '
			. implode(' ', array($type, $message, $file, $line))
		);
	}

	public function signalHandler($signal) {
		$this->logger->debug('SignalHandler got signal: ' . $signal);
		$this->shutdown();
		exit(0);
	}

	//Menus
	private function enterPin($users) {
		//Possibly replace with getData if digit input is too bothersome
		$this->nodeController->register('enterPin')
			->saySound($this->getSound('enter_pin'))
			->saySound($this->getSound('star_info'))
			->expectExactly(5)
			->maxAttemptsForInput(3)
			->cancelWith(Node::DTMF_STAR)
			->maxTimeBetweenDigits($this->config['wait_time'])
			->playOnMaxValidInputAttempts($this->getSound('contact_office'))
			->validateInputWith('pinValidation', function (Node $node) use ($users) {
				
				return $this->setActiveUser($node->getInput(), $users);// returns true if user is found in the users list with pin match
			},$this->getSound('invalid_pin')
		        )
			->addPrePromptMessage($this->getSound('welcome')); //returns void has to be last

		$this->nodeController->registerResult('enterPin')
			->onComplete()
			->jumpAfterEval(function (Node $node) {

				//Phone activity log
				$this->pushToActivityLog(array(array('source'=>Source::Phone, 'activity'=>'welcome'),
					                           array('source'=>Source::Phone, 'activity'=>'enter_pin')));
				//end Phone activity log

				$this->current_visits = $this->user->getVisits();
				$this->setClients($this->current_visits); //  to get clients names for activity log

				if (count($this->current_visits) == 1) {
					//Single Visit
					echo 'single visit';
					$this->visit = new Visit($this->logger, $this->config['api'], $this->current_visits[0]);
					$this->visit->setActive();
                   	 $menu = $this->getMenuFromStatus($this->visit->Status);
                   	 echo 'Menu is '.$menu;
                   	 return $menu;
					//return 'sayStarInfo';
				} else if (count($this->current_visits) > 1) { //Multiple Visit
					echo 'Multiple visit';
					return 'multipleVisits';			

				} else {
					//No Visits
					return 'noVisits';
				}
			});

		$this->menuRepeat('enterPin');
	}

	private function noVisits() {
		$this->nodeController->register('noVisits')
			->saySound($this->getSound('invalid_visit'))
			->executeBeforeRun(function(Node $node){
		     	//Phone activity log
				$this->pushToActivityLog(array(array('source'=>Source::Phone, 'activity'=>'invalid_visit')));			
				//end Phone activity log
		     });
	}

    private function multipleVisits() {
        $this->nodeController->register('multipleVisits')
            ->saySound($this->getSound('multiple_visits'))
            ->saySound($this->getSound('select_client'))
            ->executeBeforeRun(function (Node $node) {
            	 //setting the active visit from multiple based on user input
            	//Press 1 for Jay. Press 2 for kumara - for example

            	//Phone activity log
            	$this->pushToActivityLog(array(array('source'=>Source::Phone, 'activity'=>'multiple_visits'),
					                           array('source'=>Source::Phone, 'activity'=>'select_client')));
				//end Phone activity log

            	$count=0;
            	  foreach($this->current_visits as $visit){
                   $count++;
                    $temp_visit = new Visit($this->logger, $this->config['api'], $visit);
					$node->saySound($this->getSound('press'));
					$node->sayDigits($count);
					$node->saySound($this->getSound('for'));

					$node->saySound($this->getClient($temp_visit->ClientId));	

					//Phone activity log
					$this->pushToActivityLog(array(array('source'=>Source::Phone, 'activity'=>'press'),
					                               array('source'=>Source::Phone, 'activity'=>$count),
					                               array('source'=>Source::Phone, 'activity'=>'for'),
					                               array('source'=>Source::Phone, 'activity'=>$this->clients[$count-1]['FirstName'])));
				    //end Phone activity log				
				   }				    
			});

			 $this->nodeController->registerResult('multipleVisits')
			->onComplete()
			->jumpAfterEval(function (Node $node) {

              $visit_index = intVal($node->getInput()) - 1;
              $this->visit = new Visit($this->logger, $this->config['api'],$this->current_visits[$visit_index]);
              $this->visit->setActive();

              //Phone activity log
              $this->pushToActivityLog(array(array('source'=>Source::Phone, 'activity'=>$node->getInput())));
			 //end Phone activity log


              return $this->getMenuFromStatus($this->visit->Status);
			});
    }




	private function sayStarInfo() {
		$this->nodeController->register('sayStarInfo')
			->saySound($this->getSound('star_info'))
			->executeBeforeRun(function(Node $node){
		     	//Phone activity log
				$this->pushToActivityLog(array(array('source'=>Source::Phone, 'activity'=>'start_info')));			
				//end Phone activity log
		     });

		$this->jumpTo('sayStarInfo', 'sayVisit');
	}

	private function sayVisit() {
		$this->nodeController->register('sayVisit')
			->saySound($this->getSound('visit_for'))
			->executeBeforeRun(function (Node $node) {
				if (!$node->hasCustomData('addedName')) {
					$node->saveCustomData('addedName', true);
					$node->saySound($this->getClient($this->visit->ClientId));
				}
			});

		$this->jumpToActiveMenu('sayVisit');
	}

	private function entryMainMenu() {
		// Entry Menu
		echo  'entryMainMenu';
		$this->genericMenu('entryMainMenu', 'menu_entry')
		->executeBeforeRun(function(Node $node){
		     	//Phone activity log
				$this->pushToActivityLog(array(array('source'=>Source::Phone, 'activity'=>'menu_entry')));			
				//end Phone activity log
		     });
		$this->jumpTo('entryMainMenu', 'clockedInConfirm', 1);
		$this->jumpTo('entryMainMenu', 'missedMainMenu', 2);
		$this->jumpTo('entryMainMenu', 'reviewTaskList', 3);
		$this->menuRepeat('entryMainMenu');
	}

	private function missedMainMenu() {
		// Missed Main Menu
		$this->genericMenu('missedMainMenu', 'menu_missed')
		->executeBeforeRun(function(Node $node){
		     	//Phone activity log
				$this->pushToActivityLog(array(array('source'=>Source::Phone, 'activity'=>'menu_missed')));			
				//end Phone activity log
		     });
		$this->jumpTo('missedMainMenu', 'enterReasonCode', 1);
		$this->jumpTo('missedMainMenu', 'travelMainMenu', 2);
		$this->menuRepeat('missedMainMenu');
	}

	private function clockedIn() {
		// Clock In Confirm
		$this->genericMenu('clockedInConfirm', 'clocked_in_confirm')
			->saySound($this->getSound('yes_no'))
			->cancelWith(Node::DTMF_STAR);

		$this->nodeController->registerResult('clockedInConfirm')
			->onComplete()
			->withInput('1')
			->jumpAfterEval(function (Node $node) {
				$this->visit->ClockIn = date("Y-m-d H:i:s");
				$this->visit->Status = Status::Progress;

				//Phone activity log

				$this->pushToActivityLog(array(array('source'=>Source::Phone, 'activity'=>'clocked_in_confirm'),
					                           array('source'=>Source::Phone, 'activity'=>'yes_no'),
					                           array('source'=>Source::User, 'activity'=>'1')));
				
				//end Phone activity log
				return 'clockedIn';
			});
			

		$this->nodeController->registerResult('clockedInConfirm')
			->onComplete()
			->withInput('2')
			->jumpAfterEval(function (Node $node) {
			
				//Phone activity log
				$this->pushToActivityLog(array(array('source'=>Source::Phone, 'activity'=>'clocked_in_confirm'),
					                           array('source'=>Source::Phone, 'activity'=>'yes_no'),
					                           array('source'=>Source::User, 'activity'=>'2')));
				//end Phone activity log

				return $this->getMenuFromStatus($this->visit->Status);
			});

		$this->jumpToActiveMenu('clockedInConfirm', 2);

		$this->menuRepeat('clockedInConfirm');
		$this->menuReturn('clockedInConfirm');

		// Clock In
		$this->nodeController->register('clockedIn')
			->unInterruptablePrompts()
			->executeBeforeRun(function (Node $node) {
				$node->clearPromptMessages()
					->saySound($this->getSound('clocked_in_at'))
					->sayDateTime(time(), "IMP");

					//Phone activity log
					$this->pushToActivityLog(array(array('source'=>Source::Phone, 'activity'=>'clocked_in_at'),
					                           array('source'=>Source::Phone, 'activity'=>date('H:i:sa'))));
				   //end Phone activity log
			});

		$this->jumpToActiveMenu('clockedIn');
	}

	private function visitMainMenu() {
		// Visit Main Menu
		$this->genericMenu('visitMainMenu', 'menu_visit')
		->executeBeforeRun(function(Node $node){
		     	//Phone activity log
				$this->pushToActivityLog(array(array('source'=>Source::Phone, 'activity'=>'menu_visit')));			
				//end Phone activity log
		     });

		$this->jumpTo('visitMainMenu', 'reviewTaskList', 1);
		$this->jumpTo('visitMainMenu', 'updateTaskList', 2);
		$this->jumpTo('visitMainMenu', 'clockedOutConfirm', 3);
		$this->jumpTo('visitMainMenu', 'travelMainMenu', 4);
		$this->jumpTo('visitMainMenu', 'generalComments', 5);

		$this->menuRepeat('visitMainMenu');
	}

	private function clockedOut() {
		// Clocked Out Confirm
		$this->genericMenu('clockedOutConfirm', 'clocked_out_confirm')
			->saySound($this->getSound('yes_no'))
			->cancelWith(Node::DTMF_STAR);

		$this->nodeController->registerResult('clockedOutConfirm')
			->onComplete()
			->withInput('1')
			->jumpAfterEval(function (Node $node) {
				$this->visit->ClockOut = date("Y-m-d H:i:s");
				$this->visit->Status = Status::Finished;

				//Phone activity log
				$this->pushToActivityLog(array(array('source'=>Source::Phone, 'activity'=>'clocked_out_confirm'),
					                           array('source'=>Source::Phone, 'activity'=>'yes_no'),
					                           array('source'=>Source::User, 'activity'=>'1')));
				//end Phone activity log

				return 'clockedOut';
			});

		$this->nodeController->registerResult('clockedOutConfirm')
			->onComplete()
			->withInput('2')
			->jumpAfterEval(function (Node $node) {
			
				//Phone activity log
				$this->pushToActivityLog(array(array('source'=>Source::Phone, 'activity'=>'clocked_out_confirm'),
					                           array('source'=>Source::Phone, 'activity'=>'yes_no'),
					                           array('source'=>Source::User, 'activity'=>'2')));			
				//end Phone activity log

				return $this->getMenuFromStatus($this->visit->Status);
			});

		$this->jumpToActiveMenu('clockedInConfirm', 2);

		$this->menuRepeat('clockedOutConfirm');
		$this->menuReturn('clockedOutConfirm');

		// Clocked Out
		$this->nodeController->register('clockedOut')
			->unInterruptablePrompts()
			->executeBeforeRun(function (Node $node) {
				$node->clearPromptMessages()
					->saySound($this->getSound('clocked_out_at'))
					->sayDateTime(time(), "IMP");

				//Phone activity log
				$this->pushToActivityLog(array(array('source'=>Source::Phone, 'activity'=>'clocked_out_at'),
					                           array('source'=>Source::Phone, 'activity'=>date('H:i:sa'))));			
				//end Phone activity log
			});

		$this->jumpToActiveMenu('clockedOut');
	}

	private function completedMainMenu() {
		// Completed Main Menu
		$this->genericMenu('completedMainMenu', 'menu_complete')
		     ->executeBeforeRun(function(Node $node){
		     	//Phone activity log
				$this->pushToActivityLog(array(array('source'=>Source::Phone, 'activity'=>'menu_complete')));			
				//end Phone activity log
		     });

		$this->jumpTo('completedMainMenu', 'generalComments', 1);
		$this->jumpTo('completedMainMenu', 'travelMainMenu', 2);

		$this->menuRepeat('completedMainMenu');
	}

	private function travelMainMenu() {
		// Travel Main Menu
		$this->genericMenu('travelMainMenu', 'menu_travel')
			->cancelWith(Node::DTMF_STAR)
			->executeBeforeRun(function(Node $node){
		     	//Phone activity log
				$this->pushToActivityLog(array(array('source'=>Source::Phone, 'activity'=>'menu_travel')));			
				//end Phone activity log
		     });

		$this->jumpTo('travelMainMenu', 'enterMileage', 1);
		$this->jumpTo('travelMainMenu', 'enterTravelTime', 2);
		$this->jumpToActiveMenu('travelMainMenu', 3);

		$this->menuRepeat('travelMainMenu');
		$this->menuReturn('travelMainMenu');
	}

	private function reviewTaskList() {
		$this->nodeController->register('reviewTaskList')
			->cancelWith(Node::DTMF_STAR)
			->executeBeforeRun(function (Node $node) {
				$node->clearPromptMessages()
					->saySound($this->getSound('tasklist_for'))
					->saySound($this->getClient($this->visit->ClientId));

				//Phone activity log
				$this->pushToActivityLog(array(array('source'=>Source::Phone, 'activity'=>'tasklist_for'),
					                           array('source'=>Source::Phone, 'activity'=>$this->getClientNameById(
					                           	                                $this->visit->ClientId))));			
				//end Phone activity log

					if(count($this->visit->Tasks) == 0){
						 $node->saySound($this->getSound('noTasks'))
						      ->saySound($this->getSound('contact_office'));

						      //Phone activity log
				             $this->pushToActivityLog(array(array('source'=>Source::Phone, 'activity'=>'noTasks'),
					                           array('source'=>Source::Phone, 'activity'=>'contact_office')));
					                           //end Phone activity log
						}

				foreach ($this->visit->Tasks as $task) {
					$node->saySound($this->getSound('task'))
						->saySound($this->getTask($task['Id']));
						 //Phone activity log
				             $this->pushToActivityLog(array(array('source'=>Source::Phone, 'activity'=>'task'),
					                           array('source'=>Source::Phone, 'activity'=>$task['Text'])));
					    //end Phone activity log
				}
			});

		$this->jumpToActiveMenu('reviewTaskList');
		$this->menuReturn('reviewTaskList');

	}

	private function updateTaskList() {
		$this->nodeController->register('updateTaskList')
			->cancelWith(Node::DTMF_STAR)
			->executeBeforeRun(function (Node $node) {
				$node->clearPromptMessages()
					->saySound($this->getSound('tasklist_for'))
					->saySound($this->getClient($this->visit->ClientId));

				//Phone activity log
				 $this->pushToActivityLog(array(array('source'=>Source::Phone, 'activity'=>'tasklist_for'),
				 	                            array('source'=>Source::Phone, 'activity'=>$this->getClientNameById(
					                           	                                $this->visit->ClientId))));
			    //end Phone activity log
			});

		$this->jumpTo('updateTaskList', 'updateTask');
		$this->menuReturn('updateTaskList');

		$this->genericMenu('updateTask')
			->cancelWith(Node::DTMF_STAR)
			->executeBeforeRun(function (Node $node) {
				$taskIndex = 0;
				if ($node->hasCustomData('taskIndex')) {
					$taskIndex = $node->getCustomData('taskIndex');
				}
				//somethings gone wrong restart index
				if ($taskIndex >= count($this->visit->Tasks)) {
					$taskIndex = 0;
					$node->saveCustomData('taskIndex', 0);
				}
				$node->clearPromptMessages()
					->saySound($this->getSound('task'))
					->saySound($this->getTask($this->visit->Tasks[$taskIndex]['Id']))
					->saySound($this->getSound('task_update'));

				//Phone activity log
				 $this->pushToActivityLog(array(array('source'=>Source::Phone, 'activity'=>'task'),
				 	                            array('source'=>Source::Phone, 'activity'=>$this->visit->Tasks[$taskIndex]['Text']),
				 	                            array('source'=>Source::Phone, 'activity'=>'task_update')));
			    //end Phone activity log

				$node->saveCustomData('taskIndex', $node->getCustomData('taskIndex') + 1);
			});

		$this->nodeController->registerResult('updateTask')
			->onComplete()
			->jumpAfterEval(function (Node $node) {
				switch ($node->getInput()) {
				case '1':
				      //Phone activity log
				       $this->pushToActivityLog(array(array('source'=>Source::User, 'activity'=>'1'),
				       	                              array('source'=>Source::Phone, 'activity'=>'task_complete')));

					$this->agi->streamFile($this->getSound('task_complete'));
					$this->visit->Tasks[$node->getCustomData('taskIndex') - 1]['Status'] = TaskStatus::Complete;

					  // send curl to update VistiTask table
                     $this->task->setStatus($this->visit->Id, $this->visit->Tasks[$node->getCustomData('taskIndex') - 1]['Id'], TaskStatus::Complete);
                     // need to decide which one is the best status or answer
                     $this->task->saveAnswer($this->visit->Id, $this->visit->Tasks[$node->getCustomData('taskIndex') - 1]['Id'], AnswerStatus::Answered);
                     
					break;
				case '2':

				   //Phone activity log
				       $this->pushToActivityLog(array(array('source'=>Source::User, 'activity'=>'2'),
				       	                              array('source'=>Source::Phone, 'activity'=>'task_incomplete'),
				       	                              array('source'=>Source::Phone, 'activity'=>'record_task'),
				       	                               array('source'=>Source::User, 'activity'=>$this->getRecordFile($this->visit->Tasks[$node->getCustomData('taskIndex') - 1]['Id']))));
				    //end phone activity log

					$this->agi->streamFile($this->getSound('task_incomplete'));
					$this->agi->streamFile($this->getSound('record_task'));
					$recording = $this->record($this->visit->Tasks[$node->getCustomData('taskIndex') - 1]['Id'], $this->config['maxtime']);
				
					  // send curl to update VistiTask table
                     $this->task->setStatus($this->visit->Id, $this->visit->Tasks[$node->getCustomData('taskIndex') - 1]['Id'], TaskStatus::NotComplete);
					 $this->task->saveAnswer($this->visit->Id, $this->visit->Tasks[$node->getCustomData('taskIndex') - 1]['Id'], AnswerStatus::NotAnswered);
					break;
				default:

				 // send curl to update VistiTask table
                     $this->task->setStatus($this->visit->Id, $this->visit->Tasks[$node->getCustomData('taskIndex') - 1]['Id'], TaskStatus::NotComplete);
					 $this->task->saveAnswer($this->visit->Id, $this->visit->Tasks[$node->getCustomData('taskIndex') - 1]['Id'], AnswerStatus::Ignored);
					break;
				}
				if ($node->getCustomData('taskIndex') == count($this->visit->Tasks)) {
					return $this->getMenuFromStatus($this->visit->Status);
				} else {
					return 'updateTask';
				}
			});


		$this->nodeController->registerResult('updateTask')
			->onCancel()
			->jumpAfterEval(function (Node $node) {
			  $node->saveCustomData('taskIndex', $node->getCustomData('taskIndex') - 1);
			  return $this->getMenuFromStatus($this->visit->Status);				
			});

		$this->menuReturn('updateTask');

		$this->nodeController->register('updateTaskComplete')
			->saySound($this->getSound('task_complete'));

		$this->jumpTo('updateTaskComplete', 'updateTask');

		$this->nodeController->register('updateTaskInComplete')
			->saySound($this->getSound('task_incomplete'));

		$this->jumpTo('updateTaskInComplete', 'updateTask');

	}

	private function enterMileage() {
		$this->nodeController->register('enterMileage')
			->saySound($this->getSound('mileage'))
			->expectAtLeast(1)
			->expectAtMost(5)
			->endInputWith(Node::DTMF_HASH)
			->cancelWith(Node::DTMF_STAR)
			->maxTimeBetweenDigits($this->config['wait_time'])
			->executeBeforeRun(function(Node $node){
		     	//Phone activity log
				$this->pushToActivityLog(array(array('source'=>Source::Phone, 'activity'=>'mileage')));			
				//end Phone activity log
		     });

		$this->nodeController->registerResult('enterMileage')
			->onComplete()
			->jumpAfterEval(function (Node $node) {
				$this->visit->Mileage = (int) $node->getInput();

		     	//Phone activity log
				$this->pushToActivityLog(array(array('source'=>Source::User, 'activity'=>$this->visit->Mileage)));			
				//end Phone activity log
		   
				return 'enterMileageConfirm';
			});

		$this->menuReturn('enterMileage');

		$this->genericMenu('enterMileageConfirm')
			->cancelWith(Node::DTMF_STAR)
			->executeBeforeRun(function (Node $node) {
				$node->clearPromptMessages()
					->saySound($this->getSound('entered'))
					->sayNumber($this->visit->Mileage)
					->saySound($this->getSound('miles'))
					->saySound($this->getSound('confirm_edit'));

				//Phone activity log
				$this->pushToActivityLog(array(array('source'=>Source::Phone, 'activity'=>'entered'),
					                           array('source'=>Source::Phone, 'activity'=>$this->visit->Mileage),
					                           array('source'=>Source::Phone, 'activity'=>'miles'),
					                           array('source'=>Source::Phone, 'activity'=>'confirm_edit')));			
				//end Phone activity log
			});

		$this->jumpTo('enterMileageConfirm', 'travelMainMenu', 1);
		$this->jumpTo('enterMileageConfirm', 'enterMileage', 2);

		$this->menuRepeat('enterMileageConfirm');
		$this->menuReturn('enterMileageConfirm');
	}

	private function enterTravelTime() {
		$this->nodeController->register('enterTravelTime')
			->saySound($this->getSound('travel_time'))
			->expectAtLeast(1)
			->expectAtMost(10)
			->endInputWith(Node::DTMF_HASH)
			->cancelWith(Node::DTMF_STAR)
			->maxTimeBetweenDigits($this->config['wait_time'])
			->executeBeforeRun(function(Node $node){
		     	//Phone activity log
				$this->pushToActivityLog(array(array('source'=>Source::Phone, 'activity'=>'travel_time')));			
				//end Phone activity log
		     });

		$this->nodeController->registerResult('enterTravelTime')
			->onComplete()
			->jumpAfterEval(function (Node $node) {
				$this->visit->TravelTime = (int) $node->getInput();
				//Phone activity log
				$this->pushToActivityLog(array(array('source'=>Source::User, 'activity'=>$this->visit->TravelTime)));			
				//end Phone activity log
				return 'enterTravelTimeConfirm';
			});

		$this->menuReturn('enterTravelTime');

		$this->genericMenu('enterTravelTimeConfirm')
			->cancelWith(Node::DTMF_STAR)
			->executeBeforeRun(function (Node $node) {
				$node->clearPromptMessages()
					->saySound($this->getSound('entered'))
					->sayNumber($this->visit->TravelTime)
					->saySound($this->getSound('minutes'))
					->saySound($this->getSound('confirm_edit'));

					//Phone activity log
				$this->pushToActivityLog(array(array('source'=>Source::Phone, 'activity'=>'entered'),
					                           array('source'=>Source::Phone, 'activity'=>$this->visit->TravelTime),
					                           array('source'=>Source::Phone, 'activity'=>'minutes'),
					                           array('source'=>Source::Phone, 'activity'=>'confirm_edit')));			
				//end Phone activity log
			});

		$this->jumpTo('enterTravelTimeConfirm', 'travelMainMenu', 1);
		$this->jumpTo('enterTravelTimeConfirm', 'enterTravelTime', 2);

		$this->menuRepeat('enterTravelTimeConfirm');
		$this->menuReturn('enterTravelTimeConfirm');
	}

	private function enterReasonCode() {
		$this->nodeController->register('enterReasonCode')
			->saySound($this->getSound('reason_code'))
			->executeBeforeRun(function(Node $node){
		     	//Phone activity log
				$this->pushToActivityLog(array(array('source'=>Source::Phone, 'activity'=>'reason_code')));			
				//end Phone activity log
		     });
	}

	private function generalComments() {
		$this->nodeController->register('generalComments')
			->saySound($this->getSound('record_general'))
			->executeBeforeRun(function(Node $node){
		     	//Phone activity log
				$this->pushToActivityLog(array(array('source'=>Source::Phone, 'activity'=>'record_general')));		
				//end Phone activity log
		     });

		$this->nodeController->registerResult('generalComments')
			->onComplete()
			->jumpAfterEval(function (Node $node) {
				$recording = $this->record($this->visit->Id, $this->config['maxtime']);
				//Phone activity log
				$this->pushToActivityLog(array(array('source'=>Source::User, 'activity'=>$this->getRecordFile($this->visit->Id))));		
				//end Phone activity log
				return $this->getMenuFromStatus($this->visit->Status);
			});
	}

	//Telephony
	public function getSound($name) {
		return $this->config['sounds_path'] . $name;
	}

	public function getTask($id) {
		return $this->config['tasks_path'] . $id;
	}

	public function getClient($id) {
		return $this->config['clients_path'] . $id;
	}

	public function getRecordFile($id) {
		return $this->config['recordings_path'] . $id;
	}

	public function record($id, $maxTime = 0) {
		//filename, format, maxrecordtime in miliseconds, silencetime in seconds
		return $this->agi->record($this->getRecordFile($id), "gsm", NODE::DTMF_HASH, $maxTime * 60 * 1000, 5);
	}

	public function jumpTo($name, $dest, $input = null) {
		if (is_null($input)) {
			$this->nodeController->registerResult($name)
				->onComplete()
				->jumpAfterEval(function (Node $node) use ($dest){					
                   return $dest;				    
				});
				//->jumpTo($dest);
		} else {
			$this->nodeController->registerResult($name)
				->onComplete()
				->withInput((string) $input)
				->jumpAfterEval(function (Node $node) use ($dest, $input) {
					  //Phone activity log
				            $this->pushToActivityLog(array(array('source'=>Source::User, 'activity'=>$input)));
					 //end Phone activity log
                   return $dest;
				    
				});
				//->jumpTo($dest);
		}
	}

	public function jumpToActiveMenu($name, $input = null) {
		if (is_null($input)) {
			$this->nodeController->registerResult($name)
				->onComplete()
				->jumpAfterEval(function (Node $node) {
					return $this->getMenuFromStatus($this->visit->Status);
				});
		} else {
			$this->nodeController->registerResult($name)
				->onComplete()
				->withInput((string) $input)
				->jumpAfterEval(function (Node $node) {
					return $this->getMenuFromStatus($this->visit->Status);
				});
		}
	}

	public function genericMenu($name, $sound = null) {
		$node = $this->nodeController->register($name)
			->expectExactly(1)
			->maxAttemptsForInput($this->config['menu_repeats'])
			->maxTimeBetweenDigits($this->config['wait_time']);
		if (!is_null($sound)) {
			$node->saySound($this->getSound($sound));
		}
		return $node;
	}

	//repeat the current menu when completed with no input
	public function menuRepeat($name) {
		$this->nodeController->registerResult($name)
			->onComplete()
			->jumpTo($name);
	}

	//return to main menu on cancel
	public function menuReturn($name) {
		$this->nodeController->registerResult($name)
			->onCancel()
			->jumpAfterEval(function (Node $node) {
				return $this->getMenuFromStatus($this->visit->Status);
			});
	}

	public function setActiveUser($input, $users) {
		foreach ($users as $user) {
			if ($user['Pin'] == $input) {
				$this->user->id = $user['Id'];
				return true;
			}
		}
		return false;
	}

	public function getMenuFromStatus($status) {
		echo 'Status '.$status;
		if ($status == Status::None) {
			//Have yet to clock in
          	return 'entryMainMenu';
		} else if ($status == Status::Missed) {
			//Missed visit
			return 'missedMainMenu';
		} else if ($status == Status::Progress) {
			//Currently doing a visit
			return 'visitMainMenu';
		} else if ($status == Status::Finished) {
			//Finished a visit
			return 'completedMainMenu';
		}
		//TODO: handle QA state
	}

	private function setClients($visits)
	{
		$visit = new Visit($this->logger, $this->config['api'],array());
		$clients = $visit->getClients($visits);
		$this->clients = $clients['data']['clients'];
	}

	private function getClientNameById($id)
	{
      foreach ($this->clients as $client) {
      	if ($client['Id'] == $id)
      	  return $client['FirstName'];
      	  else
      	  	return "";
      }
	}

	private function pushToActivityLog($activityArray)
	{
		foreach($activityArray as $activity)
		array_push($this->activities, array('Source'=>$activity['source'],'Activity'=>$activity['activity'],'AtTime'=>date("Y-m-d h:i")));
	}	

}
