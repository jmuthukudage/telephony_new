<?php

require_once "Curl.php";

class Visit
{
    protected $api;
    protected $curl;
    protected $log;
    protected $fields = array();

    public function __construct($log, $api, $visit)
    {
        $this->curl = new Curl();
        $this->api = $api;
        $this->log = $log;
        foreach($visit as $key => $value) {
            array_push($this->fields, $key);
            $this->$key = $value;
        }
        //TODO: process tasks into task objects
    }

    public function setInactive()
    {
        $url = $this->api . 'visit/' . $this->Id . '/active';
        $resp = $this->curl->put_json($url, array('Active' => false));
        $this->log->debug($url);
        $this->log->debug($resp);
        $response = json_decode($resp['response'], true);
        if($response['status'] == "success"){
            return $response['data']['visits'];
        }

    }

    public function setActive()
    {
        $url = $this->api . 'visit/' . $this->Id . '/active';
        $resp = $this->curl->put_json($url, array('Active' => true));
        $this->log->debug($url);
        $this->log->debug($resp);
        $response = json_decode($resp['response'], true);
        if($response['status'] == "success"){
            return $response['data']['visits'];
        }
    }
    public function push()
    {
        $output = array();
        foreach($this->fields as $key){
            $output[$key] = $this->$key;
        }
        $url = $this->api . 'visit/' . $this->Id;
        $resp = $this->curl->put_json($url, $output);
        $this->log->debug($resp);
    }

    public function getClients($visits)
    {
        $output = array();
        foreach($visits as $visit){
            $output['clientIds'][]=$visit['ClientId'];
        }
       $url = $this->api . 'visits/clients';
       $resp = $this->curl->post_json($url, $output);

       if(isset($resp['response']))
           return json_decode($resp['response'], true);
       else
           return array();
    }

}
