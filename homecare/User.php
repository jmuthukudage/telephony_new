<?php

require_once "Curl.php";

class User
{
    public $id;
    protected $api;
    protected $curl;
    protected $log;

    public function __construct($log, $api)
    {
        $this->curl = new Curl();
        $this->api = $api;
        $this->log = $log;
    }

    public function findUsers($phone)
    {
        $url = $this->api . 'phone/' . $phone;
        $resp = $this->curl->get($url);
        $this->log->debug($url);
        $this->log->debug($resp);
        $response = json_decode($resp['response'], true);
        if($response['status'] == "success"){
            return $response['data']['users'];
        }
    }

    public function getVisits()
    {
        $url = $this->api . 'user/' . $this->id . '/visit';
        $resp = $this->curl->get($url);
        $this->log->debug($url);
        $this->log->debug($resp);
        $response = json_decode($resp['response'], true);
        if($response['status'] == "success"){
            return $response['data']['visits'];
        }
    }
}
