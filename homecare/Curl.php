<?php

class Curl
{

    protected $curlOpts = [];

    public function setCurlOpt($key, $value)
    {
        $this->curlOpts[$key] = $value;
    }

    public function setCurlOpts($data)
    {
        foreach ($data as $k => $v)
        {
            $this->curlOpts[$k] = $v;
        }
    }

    public function removeCurlOpt($key)
    {
        if (isset($this->curlOpts[$key]))
        {
            unset($this->curlOpts[$key]);
        }
    }

    public function getCurlOpts()
    {
        return $this->curlOpts;
    }

    public function clearCurlOpts()
    {
        $this->curlOpts = [];
    }

    /**
     * Use CURL to fetch a URL using GET
     * @param string $url The URL to fetch
     * @param bool $boolReturnFull [true] Return response and headers
     * @param bool $boolReturnResultsAsArray [true] Attempt to parse response into an array (when expecting JSON, for example)
     * @return string|array The return from the curl request, as a string or array (based on optional params $boolReturnFull and $boolReturnResultsAsArray)
     */
    public function get($url, $boolReturnFull = true, $boolReturnResultsAsArray = false)
    {
        $ch = curl_init($url);
        foreach ($this->curlOpts as $k => $v)
        {
            curl_setopt($ch, (gettype($k) == "string" ? constant($k) : $k) , $v);
        }

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Telephony: 1'));

        // curl_setopt($ch, CURLOPT_VERBOSE, true);
        // curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        if ($boolReturnFull !== false)
        {
            curl_setopt($ch, CURLOPT_HEADER, 1);
            $c = curl_exec($ch);
            return $this->get_headers_from_curl_response($c, $ch, $boolReturnResultsAsArray);
        }
        else
        {
            $c = curl_exec($ch);
            return $c;
        }
    }

    /**
     * Use CURL to post to a URL using POST
     * @param string $url The URL to use for POST request
     * @param array $data [array()] Optional post body, as an array
     * @param bool $boolReturnFull [true] Return response and headers
     * @param bool $boolReturnResultsAsArray [true] Attempt to parse response into an array (when expecting JSON, for example)
     * @return string|array The return from the curl request, as a string or array (based on optional params $boolReturnFull and $boolReturnResultsAsArray)
     */
    public function post_json($url, $data = array() , $boolReturnFull = true, $boolReturnResultsAsArray = false)
    {
        $ch = curl_init($url);
        foreach ($this->curlOpts as $k => $v)
        {
            curl_setopt($ch, (gettype($k) == "string" ? constant($k) : $k) , $v);
        }

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        $data_string = json_encode($data);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string),
            'X-Telephony: 1'));

        // curl_setopt($ch, CURLOPT_VERBOSE, true);
        // curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        if ($boolReturnFull !== false)
        {
            curl_setopt($ch, CURLOPT_HEADER, 1);
            $c = curl_exec($ch);
            return $this->get_headers_from_curl_response($c, $ch, $boolReturnResultsAsArray);
        }
        else
        {
            $c = curl_exec($ch);
            return $c;
        }
    }

    public function put_json($url, $data = array() , $boolReturnFull = true, $boolReturnResultsAsArray = false)
    {
        $ch = curl_init($url);
        foreach ($this->curlOpts as $k => $v)
        {
            curl_setopt($ch, (gettype($k) == "string" ? constant($k) : $k) , $v);
        }
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        $data_string = json_encode($data);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string),
            'X-Telephony: 1'));

        // curl_setopt($ch, CURLOPT_VERBOSE, true);
        // curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        if ($boolReturnFull !== false)
        {
            curl_setopt($ch, CURLOPT_HEADER, 1);
            $c = curl_exec($ch);
            return $this->get_headers_from_curl_response($c, $ch, $boolReturnResultsAsArray);
        }
        else
        {
            $c = curl_exec($ch);
            return $c;
        }
    }


    /**
     * Parse headers returned from a curl command
     * @param object $c The response from curl_exec($ch)
     * @param object $ch The curl variable after cur_init($url) and any curl_setopt(...) commands
     * @return array    An array returned form curl, which includes the content of the return, header array(s), and any error information in the following format:
     * {
     *  "response"  =>  [string = ""],
     *  "headers"   =>  [array = {}],
     *  "error"     =>
     *      {
     *      "code"  =>  [int = 0],
     *      "text"  =>  [string = ""]
     *      }
     * }
     */
    public function get_headers_from_curl_response($c, $ch, $boolReturnResultsAsArray)
    {

        // Modified from: http://stackoverflow.com/questions/10589889/returning-header-as-array-using-curl
        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $headerContent = substr($c, 0, $header_size);

        $headers = array();

        // Split the string on every "double" new line (separates internal header blocks, which might exist because of an HTTP redirect, like a 302)
        $arrRequests = explode("\r\n\r\n", $headerContent);

        for ($index = 0; $index < count($arrRequests); $index++)
        {

            // Skip blank trailing lines (at the end of the header block)
            if (empty($arrRequests[$index]) == true)
            {
                continue;
            }
            $arrHeaderSubsection = explode("\r\n", $arrRequests[$index]);


            foreach ($arrHeaderSubsection as $i => $line)
            {
                if ($i === 0)
                {
                    $headers[$index]['http_code'] = $line;
                    preg_match("/(.*?)\s{1}(.*?)\s{1}(.*)/", $line, $arrMatches);
                    $headers[$index]['http_code_array'] = array_slice($arrMatches, 1);
                }
                else
                {
                    if (stripos($line, ":") !== false)
                    {
                        list($key, $value) = explode(': ', $line);

                        if (isset($headers[$index][$key]))
                        {
                            $headers[$index][$key].= $value;
                        }
                        else
                        {
                            $headers[$index][$key] = $value;
                        }
                    }
                }
            }
        }

        $body = substr($c, $header_size);
        if ($boolReturnResultsAsArray === true)
        {
            $body = json_decode($body, true);
        }
        if (empty($headers) || curl_errno($ch) != 0)
        {
            return array(
                "response" => $boolReturnResultsAsArray ? array() : "",
                "headers" => array() ,
                "headersRaw" => array() ,
                "error" => array(
                    "code" => curl_errno($ch) ,
                    "text" => curl_error($ch)
                )
            );
        }
        return array(
            "response" => $body,
            "headers" => $headers,
            "headersRaw" => $headerContent,
            "error" => array(
                "code" => curl_errno($ch) ,
                "text" => curl_error($ch)
            )
        );
    }
}
