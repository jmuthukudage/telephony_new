<?php

require_once "Curl.php";

class Activity
{
    protected $api;
    protected $curl;
    protected $log;
    protected $activities = array();

    public function __construct($log, $api, $activity)
    {
        $this->curl = new Curl();
        $this->api = $api;
        $this->log = $log;
        foreach($activity as $key) {
            array_push($this->activities, $key);
            }
        //TODO: process tasks into task objects
    }

   public function push($visitId)
    {
        $output = array();
        $output['Activities'] = $this->activities;
        $output['VisitId'] = $visitId;
        $url = $this->api . 'visit/' . $visitId.'/activity';
        $resp = $this->curl->put_json($url, $output);
        //$this->log->debug($resp);
    }
}
